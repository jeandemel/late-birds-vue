import Vue from 'vue'
import App from './App.vue'
import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

Vue.config.productionTip = false

firebase.initializeApp({
  apiKey: "AIzaSyBgyqqVGF6_iUXVU3t-aAVHnsGI6JB0wyY",
  authDomain: "late-birds.firebaseapp.com",
  databaseURL: "https://late-birds.firebaseio.com",
  projectId: "late-birds",
  storageBucket: "late-birds.appspot.com",
  messagingSenderId: "47310751304"
});

export const db = firebase.firestore();

new Vue({
  render: h => h(App),
}).$mount('#app')
